/*
    klasa Game
*/

function Game() {

    this.userColor = null
    this.token = null
    this.numberPlayers = 0;

    var squareSize = 90
    //var remainTime = 0
    //0 to białe 1 czarne
    var szachownica = [
        [1, 0, 1, 0, 1, 0, 1, 0],  
        [0, 1, 0, 1, 0, 1, 0, 1],
        [1, 0, 1, 0, 1, 0, 1, 0],
        [0, 1, 0, 1, 0, 1, 0, 1],
        [1, 0, 1, 0, 1, 0, 1, 0],
        [0, 1, 0, 1, 0, 1, 0, 1],
        [1, 0, 1, 0, 1, 0, 1, 0],
        [0, 1, 0, 1, 0, 1, 0, 1]
    ];
    //0 to puste 2 to czarne, 1 białe
    var pionki = [
        [2, 0, 2, 0, 2, 0, 2, 0],  
        [0, 2, 0, 2, 0, 2, 0, 2],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [1, 0, 1, 0, 1, 0, 1, 0],
        [0, 1, 0, 1, 0, 1, 0, 1]
    ];
    //tablica obiektów na stronie
    var physicalPawns = []
    var scene,camera,renderer,raycaster,mouseVector, whitePawnsSelected, blackPawnsSelected, whitePawns, blackPawns
    var selectedPawn = null
    var squares = []
    //funkcja tworząca obszar gry, ustawia kamerę
    function sceneInit(){
        scene = new THREE.Scene();
        camera = new THREE.PerspectiveCamera(
            45, // kąt patrzenia kamery (FOV - field of view)
            4/3, // proporcje widoku, powinny odpowiadać proporjom naszego ekranu przeglądarki
            0.1, // minimalna renderowana odległość
            10000 // maxymalna renderowana odległość
        );
        renderer = new THREE.WebGLRenderer();
        renderer.setClearColor(0xF4F4F4);
        renderer.setSize($(window).width(), $(window).height())
        $("#root").append( renderer.domElement );
        camera.position.set(1300,1500,1300)
        camera.lookAt(scene.position)
        raycaster = new THREE.Raycaster();
        mouseVector = new THREE.Vector2()
    }
    //funkcja tworząca podstawę gry
    function baseInit(){
        var geometry = new THREE.BoxGeometry(squareSize,10,squareSize)
        var whiteBox = new THREE.MeshBasicMaterial({ 
            side: THREE.DoubleSide, 
            map: new THREE.TextureLoader().load('gfx/whiteBase.jpg') ,
            transparent: true,   
        })
        var blackBox = new THREE.MeshBasicMaterial({ 
            side: THREE.DoubleSide, 
            map: new THREE.TextureLoader().load('gfx/blackBase.jpg') ,
            transparent: true, 
        })
        for(var i=0; i< szachownica.length; i++){   //pion
            squares[i] = []
            physicalPawns[i] = []
            for(var j=0; j< szachownica[i].length; j++){    //poziom
                if(szachownica[i][j] == 0){
                    var cube = new THREE.Mesh(geometry,whiteBox)
                    cube.userData = {type: "square", color: "white", pion: i, poziom: j};
                }else{
                    var cube = new THREE.Mesh(geometry,blackBox)
                    cube.userData = {type: "square", color: "black", pion: i, poziom: j};
                }
                cube.position.set((i*squareSize)+(squareSize/2),0,(j*squareSize)+(squareSize/2))
                squares[i][j] = cube
                physicalPawns[i][j] = null
                scene.add(cube)
            }
        }
    }
    //funkcja tworząca białe pionki do gry
    function whitePawnsCreate(){
        var geometryPawns = new THREE.CylinderGeometry( squareSize/2, squareSize/2, 15, 32 );
        whitePawns = new THREE.MeshBasicMaterial({ 
            side: THREE.DoubleSide, 
            map: new THREE.TextureLoader().load('gfx/whitePawns.jpg') ,
            transparent: true,   
        })

        whitePawnsSelected = new THREE.MeshBasicMaterial({ 
            side: THREE.DoubleSide, 
            map: new THREE.TextureLoader().load('gfx/whitePawnsSelected.jpg') ,
            transparent: true,   
        })
        for(var i=0; i< pionki.length; i++){   //pion
            for(var j=0; j< pionki[i].length; j++){    //poziom
                if(pionki[i][j] == 1){
                    var cube = new THREE.Mesh(geometryPawns,whitePawns)
                    cube.position.set((i*squareSize)+(squareSize/2),10,(j*squareSize)+(squareSize/2))
                    cube.userData = {type: "pawns", color: "white", pion: i, poziom: j, damka: false};
                    physicalPawns[i][j] = cube
                    scene.add(cube)
                }
            }
        }
    }
    //funkcja tworząca pionki do gry
    function blackPawnsCreate(){
        var geometryPawns = new THREE.CylinderGeometry( squareSize/2, squareSize/2, 15, 32 );
        blackPawns = new THREE.MeshBasicMaterial({ 
            side: THREE.DoubleSide, 
            map: new THREE.TextureLoader().load('gfx/blackPawns.jpg') ,
            transparent: true, 
        })

        blackPawnsSelected = new THREE.MeshBasicMaterial({ 
            side: THREE.DoubleSide, 
            map: new THREE.TextureLoader().load('gfx/whitePawnsSelected.jpg') ,
            transparent: true,   
        })
        for(var i=0; i< pionki.length; i++){   //pion
            for(var j=0; j< pionki[i].length; j++){    //poziom
                if(pionki[i][j] == 2){
                    var cube = new THREE.Mesh(geometryPawns,blackPawns)
                    cube.position.set((i*squareSize)+(squareSize/2),10,(j*squareSize)+(squareSize/2))
                    cube.userData = {type: "pawns", color: "black", pion: i, poziom: j, damka: false};
                    physicalPawns[i][j] = cube
                    scene.add(cube)
                }
            }
        }
    }

    function localMovment(i){
        //logiczne przesunięcie
        net.updatePosition({action: "updatePosition", player: game.userColor, beforeX: selectedPawn.userData.pion, beforeY: selectedPawn.userData.poziom, newX: i.userData.pion, newY: i.userData.poziom, movmentCondition: true}) 
        if(selectedPawn.userData.color == "white")
            selectedPawn.material = whitePawns
        else
            selectedPawn.material = blackPawns
        movment(i.userData, selectedPawn)
        selectedPawn = null
        game.token = false
        dataRefresh()
    }
    //funkcja wywołana przy odpaleniu strony
    var init = function () {
        sceneInit()
        baseInit()
        function render() {
            requestAnimationFrame(render);
            renderer.render(scene, camera);
        }
        render();
    }
    //funkcja logujaca użytkownika
    this.userLogin = function(userId){
        switch(userId){
            case 1:
                this.userColor = "white"
                $('#shadowPanel').prepend($('<img>',{id:'theImg',src:'gfx/loader.gif'}))
                refreshNumberPlayers = setInterval(function(){
                    net.numberPlayers() 
                    if(game.numberPlayers == 2){
                        game.token = true
                        //moveTime()
                        clearInterval(refreshNumberPlayers);
                        $("#shadowPanel").hide()
                    } 
                }, 1000);
                $("#loginForm").hide()
                whitePawnsCreate()
                blackPawnsCreate()
                camera.position.set(1220,300,360)
                camera.lookAt(720,20,360)
                break;
            case 2:
                this.userColor = "black";
                this.token = false
                $("#shadowPanel").hide()
                whitePawnsCreate()
                blackPawnsCreate()
                camera.position.set(-500,500, 360)
                camera.lookAt(0,20,360)
                dataRefresh();
                break;
        }
    }

    this.raycaster = function (event) {
        if(!$("#shadowPanel").is(":visible") && game.token){       //sprawdzanie czy panel logowania jest ukryty
            mouseVector.x = (event.clientX / $(window).width()) * 2 - 1;
            mouseVector.y = -(event.clientY / $(window).height()) * 2 + 1;
            raycaster.setFromCamera(mouseVector, camera);
            var intersects = raycaster.intersectObjects(scene.children);
            if (intersects.length > 0) {            //kliknięcie w obiekt na stronie
                clickedEl = intersects[0].object
                if(clickedEl.userData.type == "pawns" && clickedEl.userData.color == this.userColor){
                    //powrót starego zaznaczenia do normalności
                    if(selectedPawn){
                        if(selectedPawn.userData.color == "white")
                            selectedPawn.material = whitePawns
                        else
                            selectedPawn.material = blackPawns
                    }
                    //zaznaczenie nowego elementu
                    if(clickedEl.userData.color == "white")
                        clickedEl.material = whitePawnsSelected
                    else
                        clickedEl.material = blackPawnsSelected
                    selectedPawn = clickedEl
                }else if(clickedEl.userData.type == "square"){
                    if(clickedEl.userData.color == "black" && selectedPawn){
                        if(selectedPawn.userData.poziom==clickedEl.userData.poziom-1 || selectedPawn.userData.poziom==clickedEl.userData.poziom+1){
                            if((selectedPawn.userData.pion==clickedEl.userData.pion+1 && selectedPawn.userData.color =="white")||(selectedPawn.userData.pion==clickedEl.userData.pion-1 && selectedPawn.userData.color =="black")){
                                if(pionki[clickedEl.userData.pion][clickedEl.userData.poziom] != 0){  //zbijanie do przodu
                                    console.log("znijanie do przodu")
                                }else if(pionki[clickedEl.userData.pion][clickedEl.userData.poziom] == 0){
                                    localMovment(clickedEl)
                                }else{
                                    console.log("ruch damki c")
                                }
                            }else if((selectedPawn.userData.pion==clickedEl.userData.pion-1 && pionki[clickedEl.userData.pion][clickedEl.userData.poziom] != 0) /*&& clickedEl.userData.color != selectedPawn.userData.color*/ ){
                                console.log("zbijanie do tyłu")
                            }else
                                console.log("ruch damki b")
                        }else if(selectedPawn.userData.poziom==clickedEl.userData.poziom){
                            console.log("ruch damki a")
                        }
                    }
                } 
            }else{          //odklikiwyanie elementów
                if(selectedPawn){
                    if(selectedPawn.userData.color == "white")
                        selectedPawn.material = whitePawns
                    else
                        selectedPawn.material = blackPawns
                    selectedPawn = null
                }
            }    
        } 
    }

    this.returnPawns = function (){
        return { userColor: this.userColor, pawnsTab: pionki}
    }

    this.networkMovment = function (moveInfo){
        if(moveInfo.message.movmentCondition == "true")
            movment(squares[moveInfo.message.newX][moveInfo.message.newY].userData,physicalPawns[moveInfo.message.beforeX][moveInfo.message.beforeY])
        this.token = true
        //moveTime()
    }

    var movment = function(clickSquare, movePawn){
        pionki[clickSquare.pion][clickSquare.poziom] =  pionki[movePawn.userData.pion][movePawn.userData.poziom]
        pionki[movePawn.userData.pion][movePawn.userData.poziom] = 0
        physicalPawns[clickSquare.pion][clickSquare.poziom] = physicalPawns[movePawn.userData.pion][movePawn.userData.poziom]
        physicalPawns[movePawn.userData.pion][movePawn.userData.poziom] = null
        panel.fillGrid()
        //fizyczne przesunięcie
        movePawn.position.set((clickSquare.pion * squareSize)+(squareSize/2),10,(clickSquare.poziom * squareSize)+(squareSize/2))
        movePawn.userData.pion = clickSquare.pion
        movePawn.userData.poziom = clickSquare.poziom
    }

    var dataRefresh = function(){
        if(!game.token){
            refreshData = setInterval(function(){
                net.getData()
                console.log("odpytuje: ", game.userColor) 
                if(game.token)
                    clearInterval(refreshData);
            }, 1000);
        }
    }

    // var moveTime = function(){
    //     moveRefresh = setInterval(function(){ 
    //         remainTime++
    //         console.log("move seconds: ",remainTime)
    //         if(remainTime == 21){
    //             clearInterval(moveRefresh);
    //             game.token = false
    //             //mozna dodac ewentualnie odświetlanie pionków
    //             remainTime = 0
    //             net.updatePosition({action: "updatePosition", player: game.userColor, movmentCondition: false})  
    //         }
    //     }, 1000);
    // }
    
    init();
}