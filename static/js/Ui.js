/*
    UI - obsługa interfejsu użytkownika
*/

function Ui() {
    $("#reset").on("click", function () {
        $("#login").val("")
        net.reset()
    })
    $("#loginSubmit").on("click", function () {
        net.addUser()
    })
    $(document).mousedown(function (event) {
        game.raycaster(event)
    })
    $("#arrShow").on("click", function(){
        panel.openPanel()
    })
}