/*
    obsługa komunikację Ajax - serwer
*/

function Net() {

    this.addUser = function () {
        $.ajax({
            url: "http://localhost:3000/",
            data: { login: $("#login").val(), action: "add" },
            type: "POST",
            success: function (data) {
                //czytamy odesłane z serwera dane
                var obj = JSON.parse(data)
                switch (obj.message) {
                    case "succes":
                        $("#info").text("")
                        game.userLogin(obj.id)
                        break;
                    case "repeat":
                        $("#info").text("Istnieje już użytkownik o tym loginie")
                        break;
                    case "enough":
                        $("#info").text("Jest już wystarczająca ilość użytkowników")
                        break;
                }
        
            },
            error: function (xhr, status, error) {
            },
        });
    }
    this.reset = function () {
        $.ajax({
            url: "http://localhost:3000/",
            data: { action: "reset" },
            type: "POST",
            success: function (data) {
                //czytamy odesłane z serwera dane
                var obj = JSON.parse(data)
            },
            error: function (xhr, status, error) {
                console.log(xhr);
            },
        });
    }
    this.numberPlayers = function () {
        $.ajax({
            url: "http://localhost:3000/",
            data: { action: "numberPlayers" },
            type: "POST",
            success: function (data) {
                //czytamy odesłane z serwera dane
                var obj = JSON.parse(data)
                game.numberPlayers = obj.message
            },
            error: function (xhr, status, error) {
                console.log(xhr);
            },
        });   
    }
    this.updatePosition = function (move) {
        console.log("updatePosition")
        $.ajax({
            url: "http://localhost:3000/",
            data: move,
            type: "POST",
            success: function (data) {
                //czytamy odesłane z serwera dane
                var obj = JSON.parse(data)
            },
            error: function (xhr, status, error) {
                console.log(xhr);
            },
        });
    }
    this.getData = function () {
        $.ajax({
            url: "http://localhost:3000/",
            data: { action: "getData"},
            type: "POST",
            success: function (data) {
                data = JSON.parse(data)
                if(data.message != null)
                    game.networkMovment(data)
            },
            error: function (xhr, status, error) {
                console.log(xhr);
            },
        });
    }
}