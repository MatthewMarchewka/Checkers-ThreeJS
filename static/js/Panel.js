function Panel() {
    
    function initClick(){
        //odnawianie starego diva na nowe zastosowanie
        $("#shadowPanel").empty()
        $("#shadowPanel").on("click", function(){
            $("#shadowPanel").toggle()
        })
        //dodanie contentu
        var contentDiv = $("<div>").attr('id', 'contentDiv')
        $("#shadowPanel").append(contentDiv)
        //tworzenie grida
        var gridDiv = $("<div>").attr('id', 'gridDiv')
        contentDiv.append(gridDiv)
        for(var y=0; y<8; y++){
            for(var x=0; x<8; x++){
                var partGrid = document.createElement("div")
                partGrid.id = x+"_"+y
                if( ((y % 2 == 0) && (x % 2 == 0)) || (y % 2 != 0) && (x % 2 != 0))
                    partGrid.style.backgroundColor="black";
                else
                    partGrid.style.backgroundColor="white";
                gridDiv.append(partGrid)
            }
        }
        //fill Grid
        panel.fillGrid()
    }
    this.openPanel = function(){
        if($("#loginForm").length != 0)
            initClick()
        $("#shadowPanel").toggle()
    }

    this.fillGrid = function(){
        //usuwanie poprzednich pionków
        $(".logicPawn").remove()
        // dane potrzebne do wypełnienia game.returnPawns()
        var pawns = game.returnPawns().pawnsTab
        var logicPawn = $("<div>").addClass("logicPawn")
        for(var x=0; x<pawns.length; x++){
            for(var y=0; y<pawns[x].length; y++){
                if(pawns[x][y] == 1){
                    var logicPawn = $("<div>").addClass("logicPawn")
                    logicPawn.css("background-color", "grey")
                    $("#"+y+"_"+x).append(logicPawn)
                }else if(pawns[x][y] == 2){
                    var logicPawn = $("<div>").addClass("logicPawn")
                    logicPawn.css("background-color", "red")
                    $("#"+y+"_"+x).append(logicPawn)
                }          
            }
        }
        if(game.returnPawns().userColor == "black"){
            $("#gridDiv").css("transform", "scaleY(-1)")
        }
    }
}