var http = require("http");
var fs = require("fs");
var qs = require("querystring")

var userArr = []
var moveInfo = null

function addUser(login){
    if(userArr.length < 2){ //jest mniej niż 2 userów w grze
        if(userArr.indexOf(login) == -1){   // nie ma takiego usera na serwerze
            userArr.push(login)
            response = "succes"
        }else
            response = "repeat"
    }else
        response = "enough"
    return response
}

function servResponse(req,res){
    var allData = "";
    req.on("data", function (data) {
        allData += data;
    })
    req.on("end", function (data) {
        var response = ""
        var finishObj = qs.parse(allData)
        switch (finishObj.action) {
            //dodanie nowego usera
            case "add":
                response = addUser(finishObj.login)
                break;
            case "reset":
                userArr = []
		        response = "usuniete"
                break;
            case "numberPlayers":
                response = userArr.length
                break;
            case "updatePosition":
                moveInfo = finishObj
                response = "send"
                break;
            case "getData":
                response = moveInfo
                moveInfo = null
        }
    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.end(JSON.stringify({ message: response, id: userArr.length }));
})
}

var server = http.createServer(function(request,response){
    switch (request.method) {
        case "GET":
            if (request.url === "/") {
                fs.readFile("static/index.html", function (error, data) {
                    response.writeHead(200, { 'Content-Type': 'text/html' });
                    response.write(data);
                    response.end();    
                })        
            }else if (request.url === "/css/style.css") {
                fs.readFile("static/css/style.css", function (error, data) {
                    response.writeHead(200, { 'Content-Type': 'text/css' });
                    response.write(data);
                    response.end();    
                })        
            }else if (request.url === "/js/Main.js") {
                fs.readFile("static/js/Main.js", function (error, data) {
                    response.writeHead(200, { 'Content-Type': 'text/plain' });
                    response.write(data);
                    response.end();    
                })        
            }else if (request.url === "/js/Game.js") {
                fs.readFile("static/js/Game.js", function (error, data) {
                    response.writeHead(200, { 'Content-Type': 'text/plain' });
                    response.write(data);
                    response.end();    
                })        
            }else if (request.url === "/js/Net.js") {
                fs.readFile("static/js/Net.js", function (error, data) {
                    response.writeHead(200, { 'Content-Type': 'text/plain' });
                    response.write(data);
                    response.end();    
                })        
            }else if (request.url === "/js/Ui.js") {
                fs.readFile("static/js/Ui.js", function (error, data) {
                    response.writeHead(200, { 'Content-Type': 'text/plain' });
                    response.write(data);
                    response.end();    
                })        
            }
            else if (request.url === "/libs/three.js") {
                fs.readFile("static/libs/three.js", function (error, data) {
                    response.writeHead(200, { 'Content-Type': 'text/plain' });
                    response.write(data);
                    response.end();    
                })        
            }else if (request.url === "/libs/jquery.js") {
                fs.readFile("static/libs/jquery.js", function (error, data) {
                    response.writeHead(200, { 'Content-Type': 'text/plain' });
                    response.write(data);
                    response.end();    
                })        
            }else if (request.url === "/gfx/whiteBase.jpg") {
                fs.readFile("static/gfx/whiteBase.jpg", function (error, data) {
                    response.writeHead(200, { 'Content-Type': 'image/jpeg' });
                    response.write(data);
                    response.end();    
                }) 
            }else if (request.url === "/gfx/blackBase.jpg"){
                fs.readFile("static/gfx/blackBase.jpg", function (error, data) {
                    response.writeHead(200, { 'Content-Type': 'image/jpeg' });
                    response.write(data);
                    response.end();    
                }) 
            }else if (request.url === "/gfx/whitePawns.jpg") {
                fs.readFile("static/gfx/whitePawns.jpg", function (error, data) {
                    response.writeHead(200, { 'Content-Type': 'image/jpeg' });
                    response.write(data);
                    response.end();    
                }) 
            }else if (request.url === "/gfx/whitePawnsSelected.jpg") {
                fs.readFile("static/gfx/whitePawnsSelected.jpg", function (error, data) {
                    response.writeHead(200, { 'Content-Type': 'image/jpeg' });
                    response.write(data);
                    response.end();    
                }) 
            }else if (request.url === "/gfx/blackPawns.jpg"){
                fs.readFile("static/gfx/blackPawns.jpg", function (error, data) {
                    response.writeHead(200, { 'Content-Type': 'image/jpeg' });
                    response.write(data);
                    response.end();    
                }) 
            }else if (request.url === "/gfx/blackPawnsSelected.jpg"){
                fs.readFile("static/gfx/blackPawnsSelected.jpg", function (error, data) {
                    response.writeHead(200, { 'Content-Type': 'image/jpeg' });
                    response.write(data);
                    response.end();    
                }) 
            }else if (request.url === "/gfx/loader.gif"){
                fs.readFile("static/gfx/loader.gif", function (error, data) {
                    response.writeHead(200, { 'Content-Type': 'image/gif' });
                    response.write(data);
                    response.end();    
                }) 
            }else if (request.url === "/gfx/chess.png"){
                fs.readFile("static/gfx/chess.png", function (error, data) {
                    response.writeHead(200, { 'Content-Type': 'image/png' });
                    response.write(data);
                    response.end();    
                }) 
            }else if (request.url === "/js/Panel.js") {
                fs.readFile("static/js/Panel.js", function (error, data) {
                    response.writeHead(200, { 'Content-Type': 'text/plain' });
                    response.write(data);
                    response.end();    
                })        
            }
            break;
        case "POST":
            servResponse(request,response)
            break;
    
    } 
})

server.listen(3000, function(){
   console.log("serwer startuje na porcie 3000")
});